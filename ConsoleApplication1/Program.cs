﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleApplication1.Base;
using ConsoleApplication1.Example;
using ConsoleApplication1.Example.Tasks;

namespace ConsoleApplication1
{
	public class Program
	{
		private static void Main(string[] args)
			{
				var p1 = new Planner<TaxiRideState>(2);
				var s = new TaxiRideState() {Location = 0, Cash = 100};
				var t = p1.DoPlanning(s, new List<Task<TaxiRideState>> { new TravelTask(new TravelParams(0, 10)) });
			}
	}
}
