﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication1.Base
{
	public abstract class Method<TState>
	{
		private readonly string name;

		protected Method(string name)
		{
			this.name = name;
		}

		public abstract List<Task<TState>> Tasks(TState state);

		public Type ParamsType { get; private set; }
	}
}