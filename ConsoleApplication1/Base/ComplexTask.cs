﻿using System.Collections.Generic;

namespace ConsoleApplication1.Base
{
	public class ComplexTask<TState> : Task<TState>
	{
		public ComplexTask(string name) : base(name)
		{
		}

		public override bool IsOperator
		{
			get { return false; }
		}

		public List<Method<TState>> Methods { get; private set; }

		
	}
}