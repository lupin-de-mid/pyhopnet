﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace ConsoleApplication1.Base
{
	public class Planner<TState> where TState : class
	{
		private readonly byte verbose = 2;
		private Dictionary<string, List<Method<TState>>> methodsMAp = new Dictionary<string, List<Method<TState>>>();
		private Dictionary<string, Type> typexsMap = new Dictionary<string, Type>();

		public Planner(byte verbose)
		{
			this.verbose = verbose;
			Errors = new List<string>();
		}

		public List<string> Errors { get; private set; }

		public static T DeepClone<T>(T obj)
		{
			using (var ms = new MemoryStream())
			{
				var formatter = new BinaryFormatter();
				formatter.Serialize(ms, obj);
				ms.Position = 0;

				return (T) formatter.Deserialize(ms);
			}
		}

		public List<Task<TState>> DoPlanning(TState s, List<Task<TState>> tasks)
		{
			Errors.Clear();
			var t = new List<Task<TState>>(tasks);
			var futureTask = new Stack<Task<TState>>();
			t.Reverse();
			foreach (var task in t)
			{
				futureTask.Push(task);
			}

			LogMessage(0, "verbose={0}: **\n   state = {1}\n   tasks = {2}", verbose, s, ToString(tasks));

			var resultedStack = SeekPlan(s, futureTask, new Stack<Task<TState>>(), 0);
			if (resultedStack != null)
			{
			
				var result = new List<Task<TState>>();
				while (resultedStack.Count > 0)
				{
					result.Add(resultedStack.Pop());
				}
				result.Reverse();
				LogMessage(0, "** result = {0}", ToString(result));
				return result;
			}
			else
			{
				LogMessage(0, "** result = NOT FOUND");
				return null;
			}
		}

		private Stack<Task<TState>> SeekPlan(TState state, Stack<Task<TState>> tasks, Stack<Task<TState>> plan, int depth)
		{
			LogMessage(1, "depth {0} future tasks {1}", depth, ToString(tasks));
			LogMessage(2, "depth {0} current plan {1}", depth, ToString(plan));
			if (tasks.Count == 0)
			{
				LogMessage(2, "depth {0} returns plan {1}", depth, ToString(plan));
				return plan;
			}
			var currentTask = tasks.Pop();
			if (currentTask.IsOperator)
			{
				var simpleTask = currentTask as SimpleTask<TState>;
				LogMessage(2, "depth {0} action {1}", depth, currentTask.Name);
				var newState = simpleTask.Apply(DeepClone(state));
				LogMessage(2, newState != null ? "depth {0} new state: {1}" : "no new state at depth {0}", depth, newState);
				if (newState != null)
				{
					plan.Push(currentTask);
					var solution = SeekPlan(newState, tasks, plan, depth + 1);
					if (solution != null)
					{
						return solution;
					}
					else
					{
						plan.Pop();
					}
				}
			}
			else
			{
				var complexTask = currentTask as ComplexTask<TState>;
				LogMessage(2, "depth {0} method instance {1}", depth, currentTask);
				if (methodsMAp.ContainsKey(complexTask.Name))
				{
					var methods = methodsMAp[complexTask.Name];
					if (methods == null || methods.Count == 0)
					{
						LogMessage(0, "Complex task without any method found. Task name is {0}.", complexTask.Name);
						Errors.Add(string.Format("Complex task without any method found. Task name is {0}.", complexTask.Name));
					}
					else
					{
						foreach (var method in methods)
						{
							var subTasks = method.Tasks(state);
							LogMessage(2, "depth {0} new tasks: {1}", depth, ToString(subTasks));
							if (subTasks.Count > 0)
							{
								subTasks.Reverse();
								var futureTasks = new Stack<Task<TState>>(tasks);
								foreach (var subTask in subTasks)
								{
									futureTasks.Push(subTask);
								}
								var solution = SeekPlan(state, futureTasks, plan, depth);
								if (solution != null)
								{
									return solution;
								}
							}
						}
					}
				}
				else
				{
					LogMessage(0, "Complex task without any method found. Task name is {0}.", complexTask.Name);
					Errors.Add(string.Format("Complex task without any method found. Task name is {0}.", complexTask.Name));
				}
			}
			LogMessage(2, "Depth {0}, return failure", depth);
			return null;
		}


		private static string ToString(IEnumerable<Task<TState>> tasks)
		{
			return tasks.Aggregate("", (s, s1) => s + " " + s1.Name);
		}


		private void LogMessage(byte verbose, string format, params object[] prms)
		{
			if (verbose <= this.verbose)
			{
				Console.Out.WriteLine(format, prms);
			}
		}

		public void AddMethod(Method<TState> method, string taskName)
		{
			if (!typexsMap.ContainsKey(taskName))
			{
				typexsMap[taskName] = method.GetType();
				methodsMAp[taskName] = new List<Method<TState>>();
			}
			else
			{
				if (typexsMap[taskName] != method.GetType())
				{
					throw new ArgumentException(string.Format("For this task other parameter already exist:{0}", typexsMap[taskName]));
				}
			}
			if (!methodsMAp[taskName].Contains(method))
				methodsMAp[taskName].Add(method);
		}

		public int GetTaskMethods(string taskName)
		{
			if (methodsMAp.ContainsKey(taskName))
			{
				return methodsMAp[taskName].Count;
			}
			return 0;
		}
	}
}