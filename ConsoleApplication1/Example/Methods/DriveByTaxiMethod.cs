﻿#region using

using System.Collections.Generic;
using ConsoleApplication1.Base;
using ConsoleApplication1.Example.Operators;

#endregion

namespace ConsoleApplication1.Example.Methods
{
	public class DriveByTaxiMethod : Method<TaxiRideState>
	{
		private readonly int dest;
		private readonly int location;

		public DriveByTaxiMethod(TravelParams parms)
			: base("driveByTaxi")
		{
			location = parms.Location;
			dest = parms.Dest;
		}


		public override List<Task<TaxiRideState>> Tasks(TaxiRideState state)
		{
			if (state.Cash >= TravelHelper.TaxiPrice(location, dest))
			{
				var result = new List<Task<TaxiRideState>>
				             {
					             new CallTaxiSimpleTask(location),
					             new RideTaxiSimpleTask(dest, location),
					             new PayDriverSimpleTask()
				             };
				return result;
			}
			return null;
		}
	}
}