﻿using ConsoleApplication1.Base;

namespace ConsoleApplication1.Example.Operators
{
	internal class CallTaxiSimpleTask : SimpleTask<TaxiRideState>
	{
		private readonly int location;

		public CallTaxiSimpleTask(int location) : base("callTaxi")
		{
			this.location = location;
		}

		public override TaxiRideState Apply(TaxiRideState state)
		{
			state.TaxiLocation = location;
			return state;
		}
	}
}