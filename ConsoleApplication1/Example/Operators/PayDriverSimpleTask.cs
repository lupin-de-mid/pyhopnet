﻿using ConsoleApplication1.Base;

namespace ConsoleApplication1.Example.Operators
{
	public class PayDriverSimpleTask : SimpleTask<TaxiRideState>
	{
		public PayDriverSimpleTask() : base("payDriver")
		{
		}

		public override TaxiRideState Apply(TaxiRideState state)
		{
			if (state.Cash > state.Owe)
			{
				state.Cash -= state.Owe;
				state.Owe = 0;
				return state;
			}
			return null;
		}
	}
}