﻿using ConsoleApplication1.Base;

namespace ConsoleApplication1.Example.Operators
{
	internal class WalkSimpleTask : SimpleTask<TaxiRideState>
	{
		private readonly int dest;
		private readonly int source;

		public WalkSimpleTask(int dest, int source) : base("walkByFoot")
		{
			this.dest = dest;
			this.source = source;
		}

		public override TaxiRideState Apply(TaxiRideState state)
		{
			if (state.Location == source)
			{
				state.Location = dest;
				return state;
			}
			return null;
		}
	}
}