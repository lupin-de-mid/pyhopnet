﻿using ConsoleApplication1.Base;

namespace ConsoleApplication1.Example.Operators
{
	internal class RideTaxiSimpleTask : SimpleTask<TaxiRideState>
	{
		private int dest;
		private int source;

		public RideTaxiSimpleTask(int dest, int source) : base("RideTaxi")
		{
			this.dest = dest;
			this.source = source;
		}

		public override TaxiRideState Apply(TaxiRideState state)
		{
			if (state.TaxiLocation == source &&
			    state.Location == source)
			{
				state.Owe = TravelHelper.TaxiPrice(state.Location,
					dest);
				state.TaxiLocation = dest;
				state.Location = dest;
				return state;
			}
			return null;
		}
	}
}