﻿using System;

namespace ConsoleApplication1.Example
{
	public class TaxiRideState
	{
		public int Location { get; set; }
		public double Cash { get; set; }
		public double Owe { get; set; }
		public int TaxiLocation { get; set; }

		public override string ToString()
		{
			return String.Format("Our loc {0}, Cash:{1},Owe {2},TaxiLoc{3}", Location, Cash, Owe, TaxiLocation);
		}
	}
}